/**
 * Menu Module
 *
 * @module Navigation
 * @version v0.0.1
 *
 * @author Sebastian Fitzner
 * @author Andy Gutsche
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Navigation extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {obj} obj - Object which is passed to our class
	 * @param {obj.el} obj - element which will be saved in this.el
	 * @param {obj.options} obj - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			activeClass: 'is-active',
			menuIconSel: '[data-js-atom="menu-icon"]',
		};

		super(obj, options);
		App.registerModule && App.registerModule(Navigation.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Navigation',
			version: '0.0.1',
		};
	}


	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('Navigation');
		this.$menuIcon = $(this.options.menuIconSel, this.$el);

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		this.$menuIcon.on(App.EVENTS.click, this.trigger.bind(this));
	}

	trigger(e) {
		console.log(this);
		this.$menuIcon.toggleClass(this.options.activeClass);
		App.Vent.trigger(App.EVENTS.offcanvas.toggle);
	}
}

export default Navigation;
