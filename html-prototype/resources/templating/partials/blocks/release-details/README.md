# release-details

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: release-details, @tag: block-partial }}
{{#with release-details-bp}}
	{{> b-release-details}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-release-details";
// @INSERT :: END
```
