/**
 * Description of Selector.
 *
 * @module Selector
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Selector extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {};

		super(obj, options);
		App.registerModule && App.registerModule(Selector.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Selector',
			version: '0.0.0'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('init Selector');

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		// Global events

		// Local events
	}

	/**
	 * Render class
	 */
	render() {
	}
}

export default Selector;