# offcanvas

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: offcanvas, @tag: component-partial }}
{{#with offcanvas-bp}}
	{{#wrapWith "c-offcanvas"}}
		Wrapped with markup from offcanvas.
	{{/wrapWith}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-offcanvas";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Offcanvas from './modules/offcanvas/offcanvas';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Offcanvas
 */
Helpers.loadModule({
	el: '[data-js-module="offcanvas"]',
	module: Offcanvas,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Offcanvas
 */
Helpers.loadModule({
	domName: 'offcanvas',
	module: Offcanvas,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Offcanvas
 */
EVENTS.offcanvas = {
	eventName: 'offcanvas:eventName'
};
// @INSERT :: END
```
