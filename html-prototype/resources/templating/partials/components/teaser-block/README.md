# teaser-block

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: teaser-block, @tag: component-partial }}
{{#with teaser-block-bp}}
	{{#wrapWith "c-teaser-block"}}
		Wrapped with markup from teaser-block.
	{{/wrapWith}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-teaser-block";
// @INSERT :: END
```
