# sidebar

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: sidebar, @tag: component-partial }}
{{#with sidebar-bp}}
	{{#wrapWith "c-sidebar"}}
		Wrapped with markup from sidebar.
	{{/wrapWith}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-sidebar";
// @INSERT :: END
```
