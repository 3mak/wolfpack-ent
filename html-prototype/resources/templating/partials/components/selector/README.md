# selector

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: selector, @tag: component-partial }}
{{#with selector-bp}}
	{{> c-selector}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-selector";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Selector from './modules/selector/selector';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Selector
 */
Helpers.loadModule({
	el: '[data-js-module="selector"]',
	module: Selector,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Selector
 */
Helpers.loadModule({
	domName: 'selector',
	module: Selector,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Selector
 */
EVENTS.selector = {
	eventName: 'selector:eventName'
};
// @INSERT :: END
```
