# contact-form

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: contact-form, @tag: block-partial }}
{{#with contact-form-bp}}
	{{> b-contact-form}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-contact-form";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import ContactForm from './modules/contact-form/contact-form';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init ContactForm
 */
Helpers.loadModule({
	el: '[data-js-module="contact-form"]',
	module: ContactForm,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init ContactForm
 */
Helpers.loadModule({
	domName: 'contact-form',
	module: ContactForm,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for ContactForm
 */
EVENTS.contactForm = {
	eventName: 'contactForm:eventName'
};
// @INSERT :: END
```
