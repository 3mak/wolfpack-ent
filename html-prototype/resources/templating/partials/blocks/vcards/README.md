# vcards

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: vcards, @tag: block-partial }}
{{#with vcards-bp}}
	{{> b-vcards}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-vcards";
// @INSERT :: END
```
