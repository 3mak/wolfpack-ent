# instagram-feed

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: instagram-feed, @tag: block-partial }}
{{#with instagram-feed-bp}}
	{{> b-instagram-feed}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-instagram-feed";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import InstagramFeed from './modules/instagram-feed/instagram-feed';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init InstagramFeed
 */
Helpers.loadModule({
	el: '[data-js-module="instagram-feed"]',
	module: InstagramFeed,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init InstagramFeed
 */
Helpers.loadModule({
	domName: 'instagram-feed',
	module: InstagramFeed,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for InstagramFeed
 */
EVENTS.instagramFeed = {
	eventName: 'instagramFeed:eventName'
};
// @INSERT :: END
```
