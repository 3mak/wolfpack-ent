// Main Requirements
import App from './app';
import Helpers from './utils/helpers';

// ES6 Modules
import CTA from './modules/cta/cta';
import Slider from './modules/slider/slider';
import Menu from './modules/menu/menu'
import Navigation from './modules/navigation/navigation'
import Releases from './modules/releases/releases';
import InstagramFeed from './modules/instagram-feed/instagram-feed';
import Offcanvas from './modules/offcanvas/offcanvas';
import ArticleGallery from './modules/article-gallery/article-gallery';
import ContactForm from './modules/contact-form/contact-form';
import Selector from './modules/selector/selector';
import Datelist from './modules/datelist/datelist';

// @INSERTPOINT :: @ref: js-import

// Vars
const $ = App.$;

'use strict';

// Main Functionality
class Core {
    constructor() {
        this.initialize();
    }

    /**
     * Initialize our core functionality
     * This function will only be executed once.
     */
    initialize() {
        console.log('App initialized with version: ', App.version);

        /**
         * Detect Touch
         */
        if (!App.support.touch) {
            $('html').addClass('no-touch');
        } else {

            $('html').addClass('touch');
        }

        // Redirect
        App.Vent.on(App.EVENTS.DOMredirect, (obj) => {
            if (!obj && !obj.url) throw new Error('Object is not defined. Please provide an url in your object!');

            // Redirect to page
            window.location.href = String(obj.url);
        });

        // @INSERTPOINT :: @ref: js-init-once-v3

    }

    preRender() {
        Helpers.saveDOM();
    }

    render(context) {

        /**
         * Init Offcanvas
         */
        Helpers.loadModule({
            domName: 'offcanvas',
            module: Offcanvas,
            context: context
        });


        /**
         * Init Call-To-Action
         */
        Helpers.loadModule({
            domName: 'cta',
            module: CTA,
            context: context
        });


        /**
         * Init Slider
         */
        Helpers.loadModule({
            domName: 'slider',
            module: Slider,
            context: context
        });

        /**
         * Init Menu
         */
        Helpers.loadModule({
            domName: 'menu',
            module: Menu,
            context: context
        });

        /**
         * Init Navigation
         */
        Helpers.loadModule({
            domName: 'navigation',
            module: Navigation,
            context: context
        });


        /**
         * Init Releases
         */
        Helpers.loadModule({
        	domName: 'releases',
        	module: Releases,
        	context: context
        });


        /**
         * Init InstagramFeed
         */
        Helpers.loadModule({
        	domName: 'instagram-feed',
        	module: InstagramFeed,
        	context: context
        });



/**
 * Init ContactForm
 */
Helpers.loadModule({
	domName: 'contact-form',
	module: ContactForm,
	context: context
});


/**
 * Init Selector
 */
Helpers.loadModule({
	domName: 'selector',
	module: Selector,
	context: context
});


/**
 * Init Datelist
 */
Helpers.loadModule({
	domName: 'datelist',
	module: Datelist,
	context: context
});

        // @INSERTPOINT :: @ref: js-init-v3

    }
}

document.addEventListener("DOMContentLoaded", function() {
    let core = new Core();

    /**
     * Render modules
     */
    core.preRender();
    core.render(document);

    /**
     * Initialize modules which are loaded after initial load
     * via custom event 'DOMchanged'
     */
    App.Vent.on(App.EVENTS.DOMchanged, (context) => {
        console.log('Dom has changed. Initialising new modules in: ', context);
        core.preRender();
        core.render(context);
    });
});
