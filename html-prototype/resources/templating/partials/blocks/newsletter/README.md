# newsletter

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: newsletter, @tag: block-partial }}
{{#with newsletter-bp}}
	{{> b-newsletter}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-newsletter";
// @INSERT :: END
```
