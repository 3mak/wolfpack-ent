/**
 * Description of Offcanvas.
 *
 * @module Offcanvas
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Offcanvas extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {};

		super(obj, options);
		App.registerModule && App.registerModule(Offcanvas.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Offcanvas',
			version: '0.0.0'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('init Offcanvas');

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		// Global events
		App.Vent.on(App.EVENTS.offcanvas.toggle, this.toggleOffcanvas.bind(this));
		App.Vent.on(App.EVENTS.mediachange, this.closeOffcanvas.bind(this))

		// Local events
		
	}

	toggleOffcanvas() {
		this.$el.toggleClass('is-open');
	}

	closeOffcanvas(event, data) {
		if(data.currentMedia.indexOf("mobile") < 0) {
			this.$el.removeClass('is-open');
		}

	}

	/**
	 * Render class
	 */
	render() {
	}
}

export default Offcanvas;