# social-icons

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: social-icons, @tag: block-partial }}
{{#with social-icons-bp}}
	{{> b-social-icons}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-social-icons";
// @INSERT :: END
```
