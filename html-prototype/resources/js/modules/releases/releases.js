/**
 * Release Module.
 *
 * open details view under item row
 *
 * @module Releases
 * @version v0.0.1
 *
 * @author Sven Friedemann
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Releases extends AppModule {
    /**
     * Constructor for our class
     *
     * @see module.js
     *
     * @param {Object} obj - Object which is passed to our class
     * @param {Object} obj.el - element which will be saved in this.el
     * @param {Object} obj.options - options which will be passed in as JSON object
     */
    constructor(obj) {
        let options = {
            itemSel: '[data-js-atom="release-item"]',
            itemDetailsSel: '[data-js-atom="release-details"]',
            closeButtonSel: '[data-js-atom="close-button"]',
            activeClass: "is-active",
            disabledClass: "is-disabled"
        };

        super(obj, options);
        App.registerModule && App.registerModule(Releases.info, this.el);
    }

    /**
     * Get module information
     */
    static get info() {
        return {
            name: 'Releases',
            version: '0.0.0'
        };
    }

    /**
     * Initialize the view and merge options
     *
     */
    initialize() {
        console.log('init Releases');
        this.$releaseItem = $(this.options.itemSel, this.$el);
        this.$releaseItemDetails = $(this.options.itemDetailsSel, this.$el);
        this.$closeButton = $(this.options.closeButtonSel, this.$el);
        this.allClosed = true;

        super.initialize();
    }

    /**
     * Bind events
     *
     * Listen to open and close events
     */
    bindEvents() {
        // Global events
        App.Vent.on(App.EVENTS.resize, this.render.bind(this));
        App.Vent.on(App.EVENTS.releases.closeAll, this.close.bind(this));
        // Local events
        this.$releaseItem.on(App.EVENTS.click, this.openDetails.bind(this));
        this.$closeButton.on(App.EVENTS.click, this.close.bind(this));
    }

    /**
     * Open Details
     * 
     * @param  {object} event - jQuery Event Object
     */
    openDetails(event) {
        event.preventDefault();


        let $targetItem = $(event.currentTarget);
        let $itemDetails = $targetItem.siblings(this.options.itemDetailsSel);
        this.reset();

        // Animate Height on first open
        if (this.allClosed) {
        	this.animateHeight($itemDetails);
        }

        $itemDetails.toggleClass(this.options.activeClass);
        this.$releaseItem.not($targetItem).addClass(this.options.disabledClass);

    }

    animateHeight($itemDetails) {
        $itemDetails.addClass('is-height-animated');
        this.allClosed = false;

        $itemDetails.one(App.EVENTS.transitionEnd, function() {
            $(this).removeClass('is-height-animated');
        });
    }

    /**
     * Close Click Handler
     * 
     * @param  {object} event - jQuery Event Object
     */
    close(event) {    	
        event.preventDefault();
    	let $itemDetail = $(event.currentTarget).closest(this.options.itemDetailsSel);
    	this.animateHeight($itemDetail);

        this.reset();
        this.allClosed = true;
    }

    /**
     * Reset all Items to default
     */
    reset() {
        this.$releaseItemDetails.removeClass(this.options.activeClass);
        this.$releaseItem.removeClass(this.options.disabledClass);
    }

    /**
     * Render class
     *
     * Set correct dimensions for all detail container 
     */
    render() {
        let containerWidth = this.$el.width();
        let containerOffset = this.$el.offset().left;
        let _this = this;

        this.$releaseItem.each(function() {
            let itemOffset = $(this).offset().left;
            let delta = containerOffset - itemOffset;

            let $itemDetails = $(this).siblings(_this.options.itemDetailsSel);
            $itemDetails
                .width(containerWidth)
                .css('margin-left', delta);
        });


    }
}

export default Releases;
