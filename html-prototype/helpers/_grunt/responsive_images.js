module.exports = {
	"img": {
		options: {
			separator: '--',
			sizes: [
				{
					name: 'preload',
					width: 20,
					quality: 60
				},
				{
					name: '320',
					width: 320,
					quality: 80
				},
				{
					name: '480',
					width: 480,
					quality: 80
				},
				{
					name: '640',
					width: 640,
					quality: 80
				},
				{
					name: "768",
					width: 960,
					quality: 80
				},
				{
					name: "1024",
					width: 1440,
					quality: 80
				}
			]
		},
		files: [
			{
				expand: true,
				src: ['**.{jpg,gif,png}'],
				cwd: '<%= paths.src %>/assets/img/temp/base/',
				dest: '<%= paths.src %>/assets/img/temp/pictures/'
			}
		]
	}
};