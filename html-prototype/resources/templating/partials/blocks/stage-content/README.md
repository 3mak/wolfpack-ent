# stage-content

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: stage-content, @tag: block-partial }}
{{#with stage-content-bp}}
	{{> b-stage-content}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-stage-content";
// @INSERT :: END
```
