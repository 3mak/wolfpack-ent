module.exports = {
	dist: [
		['<%= paths.dist %>/**/*', '!<%= paths.dist %>/sftp-config.json']
	],
	dev: [
		'<%= paths.dev %>/**/*'
	] 
};