# navigation

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: navigation, @tag: block-partial }}
{{#with navigation-bp}}
	{{> b-navigation}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-navigation";
// @INSERT :: END
```
