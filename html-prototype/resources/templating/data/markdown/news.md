Wolfpack Entertainment Berlin, kurz: WPE Berlin, gegründet im Januar 2008, ist ein Musiklabel, ein Management, ein Verlag, eine Bookingagentur, ein Vertrieb, eine Crew, ein weiser Großvater, eine lebenserfüllende Religion und eine große exklusive Familie. "Wir sind das nicht alles für jeden unserer Künstler, aber wir können zumindest jeden dieser Geschäftsbereiche des Musikbusinesses professionell und erfolgreich ausfüllen.

Gründer und Inhaber Danny 'D-Bo' Bokelmann bietet zusammen mit seinen Mitarbeitern talentierten Künstlern mit Wolfpack Entertainment eine Plattform, um ihre Fähigkeiten schneller, besser, fairer und lukrativer einzusetzen und zu entwickeln. Keinem Künstler werden geschäftliche oder künstlerische Marschrichtungen aufgezwungen. Professionalität, Zuverlässigkeit und vor allem das unerschütterliche Vertrauensverhältnis durch eine ehrliche und sensible Geschäftsstrategie sorgen intern wie auch zu extrenen Partnern für ein motiviertes und funktionierendes Musikbusiness-Netzwerk, das sich in den vergangenen Jahren durch eine Vielzahl von Charterfolgen, gewonnenen Preisen und Awards sowie einem allgemein sehr guten Ruf auszeichnen konnte.

WPE Berlin, das seine Wurzeln in der urbanen Musik wie HipHop und Rap hat, hat sich längst auch in den Bereichen Bass- und Club-Musik, Pop, Alternative/Indie und Rock etabliert und wird sich auch weiterhin kontinuierlich mit jedem neuen Künstler oder Trend weiterentwickeln. Entscheidender als der Musikstil sind und bleiben für Wolfpack Entertainment jedoch die Qualität und der Charakter der Künstler und ihrer Songs. "Authentisch, talentiert und leidenschaftlich sollen sie sein; großartig, erfolgreich und beständig machen wir sie dann" - Danny Bokelmann


Link Test
[Link](http://erstellbar.de) in Text
Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita corporis iusto assumenda voluptate voluptates accusantium, [doloremque](#) quaerat laboriosam! Doloremque totam debitis vitae expedita deleniti, fugit reiciendis exercitationem assumenda excepturi. Fugit.

# Header 1
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
## header 2
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
### header 3
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
#### header 5
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
##### header 6
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.