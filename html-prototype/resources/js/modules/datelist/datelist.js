/**
 * Description of Loader.
 *
 * @module Loader
 * @version v0.0.0
 *
 * @author your_name
 */

import Helpers from '../../utils/helpers';
import App from '../../app';
import AppModule from '../_global/module';
const $ = App.$;

class Datelist extends AppModule {
	/**
	 * Constructor for our class
	 *
	 * @see module.js
	 *
	 * @param {Object} obj - Object which is passed to our class
	 * @param {Object} obj.el - element which will be saved in this.el
	 * @param {Object} obj.options - options which will be passed in as JSON object
	 */
	constructor(obj) {
		let options = {
			'datelistLoaderSel': '[data-js-atom="datelist-loader"]',
			'datesContainer': '[data-js-atom="dates"]'
		};

		super(obj, options);
		App.registerModule && App.registerModule(Datelist.info, this.el);
	}

	/**
	 * Get module information
	 */
	static get info() {
		return {
			name: 'Datelist',
			version: '0.0.0'
		};
	}

	/**
	 * Initialize the view and merge options
	 *
	 */
	initialize() {
		console.log('init Datelist');
		this.$datelistLoader = $(this.options.datelistLoaderSel, this.$el);
		this.$datesContainer = $(this.options.datesContainer, this.$el);

		super.initialize();
	}

	/**
	 * Bind events
	 *
	 * Listen to open and close events
	 */
	bindEvents() {
		// Global events
		App.Vent.on(App.EVENTS.datelist.load, this.loadDates.bind(this));

		// Local events
	}

	loadDates() {
		event.preventDefault();
		this.showLoadingIcon();
		setTimeout(() => {
			let dates = this.$datesContainer.html();
			this.$datesContainer.append(dates);
			this.hideLoadingIcon();
		}, 2000);
	}

	showLoadingIcon() {
		this.$datelistLoader.addClass('is-active');
	}

	hideLoadingIcon() {
		this.$datelistLoader.removeClass('is-active');
	}

	/**
	 * Render class
	 */
	render() {
	}
}

export default Datelist;