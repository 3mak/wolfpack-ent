# archievements

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: archievements, @tag: block-partial }}
{{#with archievements-bp}}
	{{> b-archievements}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-archievements";
// @INSERT :: END
```
