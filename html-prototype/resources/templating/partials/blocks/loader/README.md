# loader

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: loader, @tag: block-partial }}
{{#with loader-bp}}
	{{> b-loader}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "/_b-loader";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Loader from './modules/loader/loader';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Loader
 */
Helpers.loadModule({
	el: '[data-js-module="loader"]',
	module: Loader,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Loader
 */
Helpers.loadModule({
	domName: 'loader',
	module: Loader,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Loader
 */
EVENTS.loader = {
	eventName: 'loader:eventName'
};
// @INSERT :: END
```
