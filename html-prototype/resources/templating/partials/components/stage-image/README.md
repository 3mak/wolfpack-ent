# stage-image

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: stage-image, @tag: component-partial }}
{{#with stage-image-bp}}
	{{> c-stage-image}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-stage-image";
// @INSERT :: END
```
