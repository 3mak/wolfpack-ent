# releases

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: releases, @tag: component-partial }}
{{#with releases-bp}}
	{{> c-releases}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-releases";
// @INSERT :: END
```

### Include: JavaScript

#### Import
``` js
// @INSERT :: START @tag: js-import //
import Releases from './modules/releases/releases';
// @INSERT :: END
```

#### Initializing in Veams V2
``` js
// @INSERT :: START @tag: js-init-v2 //
/**
 * Init Releases
 */
Helpers.loadModule({
	el: '[data-js-module="releases"]',
	module: Releases,
	context: context
});
// @INSERT :: END
```

#### Initializing in Veams V3
``` js
// @INSERT :: START @tag: js-init-v3 //
/**
 * Init Releases
 */
Helpers.loadModule({
	domName: 'releases',
	module: Releases,
	context: context
});
// @INSERT :: END
```

#### Custom Events
``` js
// @INSERT :: START @tag: js-events //
/**
 * Events for Releases
 */
EVENTS.releases = {
	eventName: 'releases:eventName'
};
// @INSERT :: END
```
