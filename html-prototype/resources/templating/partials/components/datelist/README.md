# datelist

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: datelist, @tag: component-partial }}
{{#with datelist-bp}}
	{{> c-datelist}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-datelist";
// @INSERT :: END
```
