# news-teaser

This blueprint is based on the blueprint of Veams-Components.

## Usage

### Include: Page

``` hbs
{{! @INSERT :: START @id: news-teaser, @tag: component-partial }}
{{#with news-teaser-bp}}
	{{> c-news-teaser}}
{{/with}}
{{! @INSERT :: END }}
```

### Include: SCSS

``` scss
// @INSERT :: START @tag: scss-import //
@import "components/_c-news-teaser";
// @INSERT :: END
```
